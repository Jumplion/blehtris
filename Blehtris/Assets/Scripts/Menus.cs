﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Menus : MonoBehaviour
{
    public GameObject[] menus;

	// Use this for initialization
	void Start ()
    {
        foreach (GameObject o in menus)
            o.SetActive(false);

        menus[0].SetActive(true);
    }

    public void MainMenu()
    {
        foreach (GameObject o in menus)
            o.SetActive(false);

        menus[0].SetActive(true);
    }

    public void StartGame()
    {
        foreach (GameObject o in menus)
            o.SetActive(false);

        menus[2].SetActive(true);
    }

    public void GameOver()
    {
        foreach (GameObject o in menus)
            o.SetActive(false);

        menus[1].SetActive(true);
    }

    public void Quit()
    {
        Application.Quit();
    }
}
