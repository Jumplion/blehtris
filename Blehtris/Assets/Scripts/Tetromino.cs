﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tetromino : MonoBehaviour
{
    public Color color;
    public Sprite sprt;
    private SpriteRenderer[] tiles;

    public enum TetrisType
    {
        O,
        I,
        L,
        J,
        T,
        S,
        Z
    }
    public TetrisType type;

    public int rotationIndex = 0;

    private void Awake()
    {
        tiles = transform.GetComponentsInChildren<SpriteRenderer>();
        foreach (SpriteRenderer sp in tiles)
        {
            sp.sprite = sprt;
            sp.color = color;
        }
    }

    public void SetTiles()
    {
        foreach (SpriteRenderer sprt in tiles)
        {
            GameGrid.tiles[(int)sprt.transform.position.x, (int)sprt.transform.position.y] = sprt.gameObject;
            GameGrid.tiles[(int)sprt.transform.position.x, (int)sprt.transform.position.y].GetComponent<SpriteRenderer>().color = Color.grey;
        }
    }

    public bool CheckCollision()
    {
        // If the rotation put one of the tiles out of bounds (I.E. key doesn't exist)
        // If the rotation put a tile ontop of an already occupied tile
        foreach(SpriteRenderer sprt in tiles)
        {
            // If it goes out of bounds, check if there's a nearby position where it *can* be done
            if((int)sprt.transform.position.x < 0 || (int)sprt.transform.position.x > GameGrid.Width - 1)
                return true;

            if((int)sprt.transform.position.y < 0)
                return true;

            else if (GameGrid.tiles[(int)sprt.transform.position.x, (int)sprt.transform.position.y] != null)
                return true;
        }

        // Otherwise, everything is fine
        return false;
    }

    public bool Move(Vector2 moveVect)
    {
        Vector3 prevPos = transform.position;
        transform.position = transform.position.__Z(transform.position.x + moveVect.x, transform.position.y + moveVect.y);

        if (CheckCollision())
        {
            transform.position = prevPos;
            return false;
        }

        return true;
    }

    public virtual void Rotate(bool clockwise = true)
    {
        if (type == TetrisType.O)
            return;

        Vector3 prevRot = transform.eulerAngles;
        transform.eulerAngles = transform.eulerAngles.XY_((int)(transform.eulerAngles.z + 90 * (clockwise ? -1 : 1)) % 360);

        // Need to check if rotating caused one of the tiles to move outside the grid

        if (CheckCollision())
            transform.eulerAngles = prevRot;
    }
}