﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class I : Tetromino
{
    public static int[,,] RotationMatrix =
    {
        {
            { 0, 0, 0, 0 },
            { 1, 1, 1, 1 },
            { 0, 0, 0, 0 },
            { 0, 0, 0, 0 }
        },
        {
            { 0, 0, 1, 0 },
            { 0, 0, 1, 0 },
            { 0, 0, 1, 0 },
            { 0, 0, 1, 0 }
        },
        {
            { 0, 0, 0, 0 },
            { 0, 0, 0, 0 },
            { 1, 1, 1, 1 },
            { 0, 0, 0, 0 }
        },
        {
            { 0, 1, 0, 0 },
            { 0, 1, 0, 0 },
            { 0, 1, 0, 0 },
            { 0, 1, 0, 0 }
        },
    };

    /*
    public override void Rotate(Vector2 center)
    {
        rotationIndex++;

        int[] newRotation = RotationMatrix[rotationIndex];

        int tileIndex = 0;

        // Starts at 4,4, goes to 3, 4
        for(int x = newRotation.Length - 1; x >= 0; x++)
        {

        }
    }
    */

}
