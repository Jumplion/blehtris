﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameGrid : MonoBehaviour
{
    private int endRow = 21;
    private int width = 10, height = 40;
    public static int Height, Width;

    public float stepInterval = 1.0f;
    private float deltaInterval = 0;

    public static GameObject[,] tiles;
    public GameObject gridObject;
    public GameObject[] tetrominoes;

    public Menus menus;
    public Text scoreText;

    private Vector3 spawnPoint;
    private Tetromino currentTetromino = null;

    public static bool playing = false;
    private bool crazyTime = false;

    public int score;

    void Awake()
    {
        tiles = new GameObject[width, height];
        Height = height;
        Width = width;

        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                tiles[x, y] = null;
                Instantiate(gridObject, new Vector3(x, y, 0), Quaternion.identity);
            }
        }
        spawnPoint = new Vector3(width / 2, height / 2, 0);
        SpawnTetromino(spawnPoint);
        
        Camera.main.transform.position = new Vector3(width / 2, height / 4 - 0.5f, -10);
        Camera.main.orthographicSize = (float)Mathf.Max(width, height / 2) / 2;
    }

    public void StartGame()
    {
        menus.StartGame();
        playing = true;

        score = 0;
        scoreText.text = "" + score;

        for (int y = 0; y < height; y++)
        {
            for (int x = 0; x < width; x++)
            {
                Destroy(tiles[x, y]);
                tiles[x,y]= null;
            }
        }

        SpawnTetromino(spawnPoint);
    }

    public void Update()
    {
        if (playing)
        {
            if (Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.LeftArrow))
                currentTetromino.Move(new Vector2(-1, 0));
            if (Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown(KeyCode.RightArrow))
                currentTetromino.Move(new Vector2(1, 0));
            if (Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.DownArrow))
                currentTetromino.Move(new Vector2(0, -1));
            if (Input.GetKeyDown(KeyCode.Space))
                currentTetromino.Rotate();

            deltaInterval += Time.deltaTime;

            if(deltaInterval >= stepInterval)
            {
                deltaInterval = 0;
                bool pushedDown = currentTetromino.Move(new Vector2(0, -1));

                if (!pushedDown)
                {
                    currentTetromino.SetTiles();
                    SpawnTetromino(spawnPoint);
                }

                // If we are currently applying gravity, don't spawn/push the Tetromino yet
                // Or, if it failed pushing down the currentTetromino, then we spawn a new Tetromino and set it as the current one

                int rows = CheckForCompletedRows();
                score += 100 * rows * rows;
                scoreText.text = "" + score;

                ApplyGravity();
            }
        }
    }

    public void GameOver()
    {
        menus.GameOver();
        playing = false;
    }

    public void SpawnTetromino(Vector3 spawn)
    {
        currentTetromino = Instantiate(tetrominoes[Random.Range(0, tetrominoes.Length)], spawn, Quaternion.identity).GetComponent<Tetromino>();

        // If the newly spawned tetromino is blocked from going down, Game Over
        if (currentTetromino.CheckCollision())
        {
            Destroy(currentTetromino);
            GameOver();
        }
    }

    public void ClearRow(int rowNum)
    {
        if (rowNum < 0 || rowNum > height - 1)
            return;

        for(int x = 0; x < width; x++)
        {
            Destroy(tiles[x, rowNum]);
            tiles[x, rowNum] = null;
        }
    }

    public bool ApplyGravity()
    {
        bool pushing = false;

        for (int y = 1; y < height; y++)
        {
            for (int x = 0; x < width; x++)
            {
                if (tiles[x, y] != null && tiles[x, y - 1] == null)
                {
                    tiles[x, y - 1] = tiles[x, y];
                    Destroy(tiles[x, y]);

                    tiles[x, y] = null;
                    tiles[x, y - 1].transform.position = new Vector2(x, y - 1);

                    pushing = true;
                }
            }
        }
        return pushing;
    }

    public int CheckForCompletedRows()
    {
        int rowsCleared = 0;

        for (int y = 0; y < height; y++)
        {
            for (int x = 0; x < width; x++)
            {
                if (!tiles[x, y])
                    break;
                else if (x == width - 1)
                {
                    ClearRow(y);
                    rowsCleared++;
                }
            }
        }

        return rowsCleared;
    }

    public int CheckForCompleteColumn()
    {
        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                if (tiles[x, y] != null)
                    break;
                if (y == height - 1)
                    return x;
            }
        }

        return -1;
    }
}
