﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamAnimations : MonoBehaviour
{
    public float animSpeed = 1;
    public AnimationCurve pulseCurve;
    public AnimationCurve swivelCurve;
    public AnimationCurve bounceCurve;

    private float minOrthoSize, maxOrthoSize;
    public float minAngle = 0, maxAngle = 180;
    public float minBounceDistance, maxBounceDistance;

    private Camera cam;

    // Use this for initialization
	void Start ()
    {
        cam = Camera.main;
        minOrthoSize = Camera.main.orthographicSize;
        maxOrthoSize = minOrthoSize * 2;

        //StartCoroutine(SwivelAnim());
        //StartCoroutine(PulseAnim());
        //StartCoroutine(BounceAnim());
	}
	
    public IEnumerator PulseAnim()
    {
        while (true)
        {
            float eval = pulseCurve.Evaluate(Mathf.PingPong(Time.time * animSpeed, 1));
            float size = Mathf.Lerp(minOrthoSize, maxOrthoSize, eval);
            cam.orthographicSize = size;

            yield return new WaitForSeconds(Time.deltaTime);
        }
    }

    public IEnumerator SwivelAnim()
    {
        while (true)
        {
            float eval = swivelCurve.Evaluate(Mathf.PingPong(Time.time * animSpeed, 1));
            float angle = Mathf.Lerp(minAngle, maxAngle, eval);
            cam.transform.eulerAngles = cam.transform.eulerAngles.XY_(angle);

            yield return new WaitForSeconds(Time.deltaTime);
        }
    }

    public IEnumerator BounceAnim()
    {
        while (true)
        {
            float eval = bounceCurve.Evaluate(Mathf.PingPong(Time.time * animSpeed, 1));
            float dist = Mathf.Lerp(minBounceDistance, maxBounceDistance, eval);
            cam.transform.position = cam.transform.position.X_Z(dist);

            yield return new WaitForSeconds(Time.deltaTime);
        }
    }
}
